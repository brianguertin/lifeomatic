(function ($) {
    var formatCurrency = function(amount) {
        return (amount).toLocaleString("en-US", {style: "currency", currency: "USD"})
    };
    var School = function(name, cost) {
        this.name = '' + name;
        this.pay = -(cost | 0) / 12;
        this.title = 'Student (' + name + ')';
        this.task = 'Study';
        this.go = 'Enroll';
        this.credits = 1 / 12; // 12 weeks per semester
        this.school = true;
        this.educationNeeded = 0;
    };
    var Job = function(title, pay, educationNeeded, task, education) {
        this.title = '' + title;
        this.pay = pay | 0;
        this.task = '' + task;
        this.go = 'Apply';
        this.credits = education / 12; // 12 weeks per semester
        this.educationNeeded = educationNeeded | 0;
    };
    var Home = function(name, cost) {
        this.name = '' + name;
        this.cost = cost | 0;
    };
    var homes = [
        new Home('Live with parents', 0),
        new Home('Room in shared apartment', 600),
        new Home('Studio apartment', 900),
        new Home('One-bedroom', 1400),
        new Home('Small House', 2000)
    ];
    var jobs = [
        new Job('Unemployed / Retired', 0, 0, 'Live', 0.01),
        new Job('Grocery Store Clerk', 10, 0, 'Bag Items', 0.1),
        new Job('Fast Food Cook', 10, 0, 'Flip Burgers', 0.1),
        new Job('Retail Store Manager', 15, 4, 'Manage', 0.2),
        new Job('Software Engineer Intern', 15, 12, 'Code', 0.5),
        new Job('Jr. Software Engineer', 40, 48, 'Code', 0.5),
        new Job('Sr. Software Engineer', 90, 96, 'Code', 0.5),
        new Job('Lead Software Engineer', 120, 128, 'Code', 0.5),
        new Job('Web Designer', 25, 24, 'Design', 0.5),
        new Job('Jr. Data Analyst', 30, 12, 'Analyze', 0.5),
        new Job('Jr. Data Scientist', 40, 36, 'Science', 0.5),
        new Job('Sr. Data Scientist', 80, 80, 'Science', 0.5),
        new Job('Lead Data Scientist', 100, 128, 'Science', 0.5),
        new Job('Product Manager', 45, 48, 'Manage', 0.5),
        new School('Self-Taught', 0),
        new School('Community College', 160),
        new School('Ivy League', 1300)
    ];
    jobs[0].go = 'Resign';
    var life = {
        age: {years:18, months:0, days:0},
        cash: 0,
        debt: 0,
        education: 0,
        job: null,
        home: homes[0]
    };
    var tick = function() {
        life.age.days += 1;
        if (life.age.days > 30) {
            life.age.days = 0;
            life.age.months += 1;
        }
        if (life.age.months > 12) {
            life.age.months = 0;
            life.age.years += 1;
        }
        var rentPayment = life.home.cost / 30;
        if (life.cash >= rentPayment) {
            life.cash -= rentPayment;
        } else {
            var rentOverdue = rentPayment - life.cash;
            life.cash = 0;
            life.debt += rentOverdue;
        }
        if (life.cash > 0 && life.debt > 0) {
            var debtPayment = Math.min(life.debt, life.cash, Math.max(25, Math.min(life.cash / 12, life.debt / 6)));
            life.cash -= debtPayment;
            life.debt -= debtPayment;
        }
        if (life.debt > 0) {
            life.debt += Math.floor(life.debt * (0.04 / 365) * 100) / 100;
        }
    };
    var dom = {
        age: $('#age'),
        cash: $('#cash'),
        debt: $('#debt'),
        job: $('#job'),
        home: $('#home'),
        education: $('#education'),
        homeChange: $('#home-change'),
        homePanel: $('#home-panel'),
        jobsPanel: $('#jobs-panel'),
        jobChange: $('#job-change'),
        workTask: $('#work-task'),
        newJobs: $('#new-jobs')
    };
    var setJob = function(job) {
        life.job = job;
        dom.jobsPanel.hide();
        dom.workTask.text(job.task);
    };
    setJob(jobs[0]);
    var jobsUnlocked = $(jobs).filter(function(){return this.educationNeeded <= life.education}).length;
    dom.jobChange.on('click', function() {
        dom.jobsPanel.toggle();
        dom.newJobs.hide();
        if (dom.jobsPanel.is(':visible')) {
            dom.jobsPanel.html('');
            $(jobs).each(function() {
                var job = this;
                if (job == life.job || job.educationNeeded > life.education) {
                    return;
                }
                var li = $('<li>');
                if (job.school) {
                    li.text(job.title + ', ' + formatCurrency(-job.pay*12) + '/credit ');
                } else {
                    li.text(job.title + ', ' + formatCurrency(job.pay) + '/hr ');
                }
                var a = $('<a href="#">' + job.go + '</a>');
                a.on('click', function() {
                    setJob(job);
                    return false;
                });
                li.append(a);
                dom.jobsPanel.append(li);
            });
        }
        return false;
    });
    dom.homeChange.on('click', function() {
        dom.homePanel.toggle();
        if (dom.homePanel.is(':visible')) {
            dom.homePanel.html('');
            $(homes).each(function() {
                var home = this;
                if (home == life.home) {
                    return;
                }
                var li = $('<li>');
                li.text(home.name + ', ' + formatCurrency(home.cost) + '/month ');
                var a = $('<a href="#">Move</a>');
                a.on('click', function() {
                    life.home = home;
                    dom.homePanel.hide();
                    dom.home.text(job.home.name);
                    return false;
                });
                li.append(a);
                dom.homePanel.append(li);
            });
        }
        return false;
    });
    dom.workTask.on('click', function() {
        if (life.job.pay < 0) {
            life.debt -= life.job.pay;
        } else {
            life.cash += life.job.pay;
        }
        if (life.job.credits) {
            life.education += life.job.credits;
        }
        var prevJobsUnlocked = jobsUnlocked;
        jobsUnlocked = $(jobs).filter(function(){return this.educationNeeded <= life.education}).length;
        if (jobsUnlocked > prevJobsUnlocked) {
            dom.newJobs.show();
        }
        return false;
    });
    var lastDayTime = 0;
    var render = function () {
        requestAnimationFrame(render);
        var thisDayTime = new Date().getTime();
        if (thisDayTime - lastDayTime > 1000) {
            lastDayTime = thisDayTime;
            tick();
        }
        dom.age.text(life.age.years + ' years, ' + life.age.months + ' months, ' + life.age.days + ' days');
        dom.cash.text(formatCurrency(life.cash));
        dom.debt.text(formatCurrency(life.debt));
        dom.job.text(life.job.title);
        dom.home.text(life.home.name);
        dom.education.text(life.education.toFixed(2) + ' credits');
    };
    requestAnimationFrame(render);
})(jQuery);